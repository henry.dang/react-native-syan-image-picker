//
//  TZAuthLimitedHeaderTipView.h
//  TZImagePickerController
//
//  Created by Henry on 2022/8/29.
//  Copyright © 2022 谭真. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TZAuthLimitedHeaderTipView : UIView

/// 相册权限为“选择部分照片”时，照片选择界面提示文字
@property (nonatomic, copy) NSString *authLimitedTipTitle;
/// 相册权限为“选择部分照片”时，照片选择界面设置文字
@property (nonatomic, copy) NSString *authLimitedTipSetting;
/// 设置按钮点击事件
@property (nonatomic, copy) void (^settingButtonDidClicked)(void);

@end

NS_ASSUME_NONNULL_END
