//
//  TOCropModel.h
//  RNSyanImagePicker
//
//  Created by Henry on 2022/11/10.
//  Copyright © 2022 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

NS_ASSUME_NONNULL_BEGIN

@interface TOCropModel : NSObject

@property (nonatomic, strong) PHAsset *asset;
@property (nonatomic, assign) BOOL showCropCircle;
@property (nonatomic, assign) CGFloat cropViewW;
@property (nonatomic, assign) CGFloat cropViewH;
@property (nonatomic, strong) UIImage *photo;
@property (nonatomic, assign) BOOL freeStyleCropEnabled;

@end

NS_ASSUME_NONNULL_END
